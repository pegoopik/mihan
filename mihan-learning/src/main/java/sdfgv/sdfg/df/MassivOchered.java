package sdfgv.sdfg.df;
class Queue{
   private char q[];
    private int putloc, getloc;

    Queue(int size){
        q = new char[size+1];
        putloc = getloc = 0;
    }
// поместить символ в очередь
    void put(char ch){
        if(putloc==q.length-1){
            System.out.println(" - Очередь заполнена");
            return;
        }
        putloc++;
        q[putloc] = ch;
    }
// извлеч символ из очереди
    char get(){
        if(getloc == putloc){
            System.out.println(" - Очередь пуста");
            return (char) 0;
        }
        getloc++;
        return q[getloc];
    }
}
public class MassivOchered {
    public static void main(String[] args) {
        Queue bigQ = new Queue (100);
        Queue smallQ = new Queue(4);
        char ch;
        int i;

        System.out.println("Использование очереди bigQ для сохранения алфавита");
        for(i=0; i<26; i++)
            bigQ.put((char)('A'+i));
        for (i=0; i<26; i++) {
            ch = bigQ.get();
            if (ch != (char) 0) System.out.print(ch);
        }
        System.out.println("\n");
        System.out.println("Использование очеренди smallQ для генерации ошибок");
        for(i=0; i<5; i++){
            System.out.print("Попытка сохранения " + (char) ('Z' - i));
            smallQ.put((char) ('Z' - i));
            System.out.println();
        }
        System.out.println();
        // дополнительные ошибки при обращении к очереди smallQ
        System.out.println("Содержимое smallQ: ");
        for(i=0; i<5; i++){
            ch = smallQ.get();
            if(ch != (char) 0) System.out.println(ch);
        }
    }
}


// помещаем и извлекаем очередь из массивов