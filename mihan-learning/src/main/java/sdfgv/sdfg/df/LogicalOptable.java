package sdfgv.sdfg.df;

public class LogicalOptable {
    public static void main(String[] args)
    throws java.io.IOException {
        char ch, ignore, ansver = 's';

        do {
            System.out.println("Задумана буква из диапазона a-z.");
            System.out.print("Попытайтесь ее угадать");

            ch = (char) System.in.read();
            do {
                ignore = (char) System.in.read();
            } while(ignore != '\n');
            if (ch == ansver)
                System.out.println("Правильно " + ch);
            else{
                if (ch < ansver)
                    System.out.println("Ближе к концу алфавита");
                else
                    System.out.println("Ближе к началу алфавита");
                System.out.println("Повторите попытку\n");
            }
        }while (ansver != ch);
    }
}
